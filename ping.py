import pandas as pd
import socket

file_name = "Video_Sitelist_Tier1 (1).xlsx"
domain_names = list()
xl = pd.ExcelFile(file_name)
sheets = xl.sheet_names
for sheet in sheets:
    df = xl.parse(sheet)
    values = df[df.columns[0]].values
    for value in values:
        try:
            socket.gethostbyname(value)
        except:
            pass
        else:
            if value not in domain_names:
                 domain_names.append(value)

print(domain_names)

