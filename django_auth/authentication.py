from rest_framework import authentication

from newsfeedapp import models


class TokenAuthentication(authentication.TokenAuthentication):
    model = models.Token
