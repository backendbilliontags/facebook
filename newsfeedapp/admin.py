from django.contrib import admin

from newsfeedapp import models


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'dob', 'gender', 'profile_pic')


class NewsFeedAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'title', 'description', 'created_on', 'updated_on', 'is_active',
                    'comment_count', 'like_count')


class NewsFeedFeedImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'news_feed', 'image')


class NewsFeedCommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'news_feed', 'comment', 'is_active')


class NewsFeedLikeAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'news_feed', 'created_on')


admin.site.register(models.UserProfile, UserProfileAdmin)
admin.site.register(models.NewsFeed, NewsFeedAdmin)
admin.site.register(models.NewsFeedImage, NewsFeedFeedImageAdmin)
admin.site.register(models.NewsFeedComments, NewsFeedCommentAdmin)
admin.site.register(models.NewsFeedLike, NewsFeedLikeAdmin)
