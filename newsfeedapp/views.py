from django.db import transaction
from rest_framework import generics, response, permissions, exceptions, viewsets

from newsfeedapp import serializer
from newsfeedapp.models import NewsFeedComments, NewsFeed, UserProfile, NewsFeedImage


class CreateUserProfileView(generics.CreateAPIView):
    """
    use this endpoint create user profile
    """
    serializer_class = serializer.UserProfileSerializer


class CreateNewsFeedPostView(generics.CreateAPIView):
    """
        use this endpoint create news feed
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    serializer_class = serializer.NewsFeedPostSerializer

    @transaction.atomic()
    def perform_create(self, serializer):
        news_feed = serializer.save()

        images = self.request.data.getlist('feed_image')
        for image in images:
            NewsFeedImage.objects.create(news_feed=news_feed, image=image)


class UpdateUserProfileView(generics.UpdateAPIView):
    """
        use this endpoint update user profile
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    queryset = UserProfile.objects.all()
    serializer_class = serializer.UserProfileSerializer

# TODO update news feed values


class UpdateNewsFeedView(generics.UpdateAPIView):
    """
        use this endpoint update new feed
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    queryset = NewsFeed.objects.all()
    serializer_class = serializer.NewsFeedPostSerializer

    @transaction.atomic()
    def perform_update(self, serializer):
        news_feed = serializer.save()

        images = self.request.data.getlist('feed_image')
        for image in images:
            NewsFeedImage.objects.create(news_feed=news_feed, image=image)


class DeleteNewsFeedView(generics.GenericAPIView):
    """
    use this endpoint delete news feed
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def delete(self, request, pk):
        try:
            news = NewsFeed.objects.get(pk=pk)
        except:
            return response.Response({"none_field_error": "Invalid Id"})
        news.is_active = not news.is_active
        news.save()
        return response.Response({"NewsFeed": "active or inactive"})


class ListNewsFeedView(generics.ListAPIView):
    """
    use this endpoint list news feed
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializer.NewsFeedPostSerializer
    queryset = NewsFeed.objects.all()


class RetrieveNewsFeedView(generics.RetrieveAPIView):
    """
    use this endpoint Retrieve news feed
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializer.NewsFeedPostSerializer
    queryset = NewsFeed.objects.all()


class CreateCommentsView(generics.CreateAPIView):
    """
    use this endpoint create comments
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializer.NewsFeedCommentsSerializer


class ListCommentView(generics.ListAPIView):
    """
    use this endpoint list comments
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializer.NewsFeedCommentsSerializer
    queryset = NewsFeedComments.objects.all()


class DeleteCommentView(generics.GenericAPIView):
    """
    use this endpoint delete comment
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def delete(self, request, pk):
        news = NewsFeedComments.objects.get(pk=pk)
        if news.is_active:
            news.is_active = False
            news.save()
            return response.Response({"data": "deleted"})
        else:
            return response.Response({"non_field_error": "Invalid"})


class LikeCommentView(generics.CreateAPIView):
    """
    use this endpoint like or dislike comment
    """
    serializer_class = serializer.NewsFeedLikeSerializer


class NewsFeedViewSet(viewsets.ModelViewSet):
    """
    use this endpoint viewing and editing
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    queryset = NewsFeed.objects.all()
    serializer_class = serializer.NewsFeedPostSerializer


class NewsFeedCommentsViewSet(viewsets.ModelViewSet):
    """
    use this endpoint viewing and editing
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    queryset = NewsFeedComments.objects.all()
    serializer_class = serializer.NewsFeedCommentsSerializer
