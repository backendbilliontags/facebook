from annoying.functions import get_object_or_None
from django.contrib.auth.models import User
from django.db import transaction
from rest_framework import serializers

from .models import NewsFeed, NewsFeedImage, NewsFeedComments, UserProfile, NewsFeedLike


class UserProfileSerializer(serializers.ModelSerializer):
    gender = serializers.CharField(max_length=1)
    dob = serializers.DateField()
    profile_pic = serializers.ImageField()

    class Meta:
        model = UserProfile
        fields = ('user', 'gender', 'dob', 'profile_pic')


class ListUserProfileSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'profile')


class NewsFeedImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsFeedImage
        fields = ('id', 'image')


class NewsFeedCommentsSerializer(serializers.ModelSerializer):
    user = ListUserProfileSerializer(read_only=True)
    is_active = serializers.BooleanField(default=True)

    class Meta:
        model = NewsFeedComments
        fields = ('news_feed', 'user', 'id', 'comment', 'created_on', 'is_active')

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return self.Meta.model.objects.create(**validated_data)


class NewsFeedLikeSerializer(serializers.ModelSerializer):
    user = ListUserProfileSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = NewsFeedLike
        fields = ('id', 'news_feed', 'user', 'user_id')

    @transaction.atomic()
    def create(self, validated_data):

        like = get_object_or_None(NewsFeedLike, user=validated_data['user_id'], news_feed=validated_data['news_feed'])
        if like:
            like.delete()
        else:
            like = NewsFeedLike.objects.create(user_id=validated_data['user_id'], news_feed=validated_data['news_feed'])
        return like


class NewsFeedPostSerializer(serializers.ModelSerializer):
    feed_image = NewsFeedImageSerializer(many=True, read_only=True)
    comments = NewsFeedCommentsSerializer(many=True, read_only=True)
    likes = NewsFeedLikeSerializer(many=True, read_only=True)
    images = serializers.ImageField(write_only=True, required=False)

    class Meta:
        model = NewsFeed
        fields = ('id', 'title', 'description', 'feed_image', "images", 'comments', 'likes')

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return self.Meta.model.objects.create(**validated_data)
