from django.apps import AppConfig


class NewsfeedappConfig(AppConfig):
    name = 'newsfeedapp'
