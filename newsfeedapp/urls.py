from django.urls import path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register("news_feed", views.NewsFeedViewSet)
router.register("comments", views.NewsFeedCommentsViewSet)

urlpatterns = [
    # path('create/user_profile/', views.CreateUserProfileView.as_view(), name='create_user_profile'),
    # path('update/user_profile/<int:pk>/', views.UpdateUserProfileView.as_view(), name='update_user_profile'),
    #
    # path('post/news-feed/', views.CreateNewsFeedPostView.as_view(), name='post_news_feed'),
    # path('delete/news-feed/<int:pk>/', views.DeleteNewsFeedView.as_view(), name='delete_news_feed'),
    # path('list/news-feeds/', views.ListNewsFeedView.as_view(), name='list_news_feed'),
    # path('retrieve/news-feed/<int:pk>/', views.RetrieveNewsFeedView.as_view(), name='list_news_feed'),
    # path('update/news-feed/<int:pk>/', views.UpdateNewsFeedView.as_view(), name='update_news_feed'),
    #
    # path('post/comment/', views.CreateCommentsView.as_view(), name='create_comment'),
    # path('list/comments/', views.ListCommentView.as_view(), name='list_comment'),
    # path('delete/comment/<int:pk>/', views.DeleteCommentView.as_view(), name='delete_comment'),
    #
    # path('like_or_dislike/comment/', views.LikeCommentView.as_view(), name='like_dislike_comment'),

] + router.urls
