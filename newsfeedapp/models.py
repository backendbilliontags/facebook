import binascii
import os

from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    objects = None
    GENDER_CHOICE = (
        ('M', 'MALE'),
        ('F', 'FEMALE')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    dob = models.DateField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICE)
    profile_pic = models.ImageField(upload_to='images')

    class Meta:
        db_table = "user_profile"
        verbose_name = "User Profile"
        verbose_name_plural = "User Profile"


class NewsFeed(models.Model):
    objects = None
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=30, blank=False)
    description = models.TextField(max_length=250, blank=False)
    is_active = models.BooleanField(default=True)
    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "news_feeds"
        ordering = '-created_on',

    def comment_count(self):
        return self.comments.count()

    def like_count(self):
        return self.likes.count()


class NewsFeedImage(models.Model):
    objects = None
    news_feed = models.ForeignKey(NewsFeed, on_delete=models.CASCADE, related_name='feed_image')
    image = models.ImageField(upload_to="images")

    class Meta:
        db_table = "feed_images"


class NewsFeedComments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    news_feed = models.ForeignKey(NewsFeed, on_delete=models.CASCADE, related_name='comments')
    comment = models.CharField(max_length=250)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "feed_comments"


class NewsFeedLike(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_like')
    news_feed = models.ForeignKey(NewsFeed, on_delete=models.CASCADE, related_name='likes')
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "feed_like"
        ordering = 'user',


class Token(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='auth_tokens')
    client = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'multi_token'
        unique_together = ('user', 'client',)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key
        return super(Token, self).save(*args, **kwargs)

    @property
    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    LOGIN_FIELDS = (
        'client',
    )
