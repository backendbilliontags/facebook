# Generated by Django 2.0.4 on 2018-04-04 13:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsfeedapp', '0011_auto_20180404_1259'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='newsfeedlike',
            unique_together=set(),
        ),
    ]
