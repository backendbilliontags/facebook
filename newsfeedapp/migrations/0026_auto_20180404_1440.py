# Generated by Django 2.0.4 on 2018-04-04 14:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsfeedapp', '0025_auto_20180404_1439'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userprofile',
            options={'verbose_name': 'User Profile', 'verbose_name_plural': 'User Profiles'},
        ),
    ]
