from django.contrib.auth.models import User
from django.db import models


class StudentDetails(models.Model):
    objects = None
    GENDER_CHOICE = (
        ('M', 'MALE'),
        ('F', 'FEMALE')
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    roll_number = models.IntegerField()
    department = models.CharField(max_length=30)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICE)
    dob = models.DateField()
    address = models.CharField(max_length=256)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "student_details"
        verbose_name = "Student Detail"

