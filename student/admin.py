from django.contrib import admin
from student import models


class StudentDetailsAdmin(admin.ModelAdmin):
    list_display = ("name", "roll_number", "gender", "department", "dob", "address", "is_active")
    search_fields = ('roll_number',)


admin.site.register(models.StudentDetails, StudentDetailsAdmin)
