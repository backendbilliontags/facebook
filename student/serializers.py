from rest_framework import serializers
from student import models


class StudentDetailsSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    class Meta:
        model = models.StudentDetails
        fields = ("id", "name", "roll_number", "department", "dob", "gender", "address", "is_active")

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return self.Meta.model.objects.create(**validated_data)
