# Generated by Django 2.0.4 on 2018-04-06 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StudentDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('roll_number', models.IntegerField()),
                ('department', models.CharField(max_length=30)),
                ('gender', models.CharField(choices=[('M', 'MALE'), ('F', 'FEMALE')], max_length=1)),
                ('dob', models.DateField()),
            ],
            options={
                'verbose_name': 'Student Detail',
                'db_table': 'student_details',
            },
        ),
    ]
