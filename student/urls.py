from django.urls import path
from rest_framework import routers

from student import views

router = routers.SimpleRouter()
router.register("student", views.StudentDetailsViewSet)

urlpatterns = [

] + router.urls
