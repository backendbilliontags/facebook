import csv
import urllib
from urllib.parse import urlencode
import random
from urllib.request import urlopen


from django.http import HttpResponse
from rest_framework import viewsets, exceptions, response, permissions
from rest_framework.decorators import action, api_view

from student import serializers
from student.models import StudentDetails


class StudentDetailsViewSet(viewsets.ModelViewSet):
    """
    use this endpoint viewing and editing
    1. create student details
    2. List all student details
    3. Retrieve student details
    4. Update student details
    5. Hard delete student details.
    6. Active toggle Student details.
    7. Export all Student details.
    """
    # permission_classes = (
    #     permissions.IsAuthenticated,
    # )

    model = StudentDetails
    queryset = StudentDetails.objects.all()
    serializer_class = serializers.StudentDetailsSerializer

    @action(detail=False)
    def export_users_csv(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'filename="student_Details.csv"'

        writer = csv.writer(response)
        writer.writerow(['Name', 'Roll_number', 'Department', 'Gender', 'Dob', 'Address'])

        student = StudentDetails.objects.all().values_list('name', 'roll_number', 'department',
                                                           'gender', 'dob', 'address')
        for user in student:
            writer.writerow(user)

        return response

    @action(detail=True)
    def safe_delete(self, request, pk):
        student = self.int_to_student(pk)
        student.is_active = not student.is_active
        student.save()
        serializer = self.get_serializer(student, context={"request": request}).data
        return response.Response(serializer)

    def int_to_student(self, pk):
        try:
            return self.model.objects.get(pk=pk)
        except:
            raise exceptions.NotFound()

    @action(methods=["POST"], detail=False)
    def read_file(self, request):
        file = request.data['csv_file'].read().decode("utf-8").splitlines()
        result = csv.DictReader(file)
        return response.Response(result)



    @action(detail=False)
    def send_message_view(self, request):
        one_time_password = random.randint(10 ** (6 - 1), 10 ** 6+1)
        message = "{} This is your BLUEDIO verification code.".format(one_time_password)
        details = {
            'authkey': "115404ALmB9m7M57c3d8aa", 'mobiles': request.GET["mobiles"], 'message': message,
            'sender': "BLUDIO", 'route': "4"
        }
        url = "http://api.msg91.com/api/sendhttp.php"
        data = urlencode(details).encode('utf-8')
        req = urllib.request.Request(url, data)
        urllib.request.urlopen(req)
        return response.Response({"OTP": "Send"})